var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var data = require('../config/data');

var postSchema = new Schema({
    _id: {type: String, required: true}, // Id
    creator: {type: Object, required: true}, // Creator
    content: {type: String, required: true}, // content
    replies: {type: Array, default: new Array()}, // replies to the post
    likes: {type: Array, default: new Array()}, // likes
    createdAt: {type: String, default: data.getDate}, // createdAt as String
    createNumber: {type: Number, default: Date.now()}, // createNumber as Number
    images: {type: Array, default: new Array()}, // posted images
    videos: {type: Array, default: new Array()}, // posted videos
});

module.exports = mongoose.model('Post', postSchema);
