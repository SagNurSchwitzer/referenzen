var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var data = require('../config/data');

var postSchema = new Schema({
    token: {type: String, required: true}, // Id
    userID: {type: String, required: true}, // User id
});

module.exports = mongoose.model('Token', postSchema);
