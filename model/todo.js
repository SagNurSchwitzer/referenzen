var mongoose = require('mongoose');
var data = require('../config/data');
var Schema = mongoose.Schema;

var schema = new Schema({
    _id: {type: String, default: data.getNewUUID()},
    content: {type: String, required: true},
    date: {type: String, default: data.getDate()},
    createdAt: {type: String, required: true},
    who: { type: Object, required: true},
    from: { type: Object, required: true},
    done: { type: Boolean, default: false},
});

module.exports = mongoose.model('Todo', schema);
