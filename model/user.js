var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');
var data = require('../config/data');

var userSchema = new Schema({
    _id: {type: String, required: true}, // Id
    name: {type: String, required: true}, // name
    username: {type: String, required: true}, // Username
    description: {type: String, default: ""}, // description
    email: {type: String, required: true}, // Email
    password: {type: String, required: true}, // password
    follower: {type: Array, default: new Array()}, // follower
    followed: {type: Array, default: new Array()}, // followed
    image: {type: String, default: 'https://image.woidbook.com/uploads/pb-images/default.png'}, // Profile image
    createdAt: {type: Number, default: Date.now()}, // createdAt as Number
    lastOnline: {type: String, default: data.getDate}, // lastOnline as String
    role: {type: String, default: "Member"}, // role
    address: {type: Object, default: { town: " ", country: " "}}, // address
    birthday: {type: String, default: ""}, // birthday
    settings: {type: Object, default: { privacy: { seePosts: 'all', canComment: 'all' }}}, // settings
    notifications: {type: Array, default: new Array()}, // Notifications
    blocked: {type: Array, default: new Array()}, // blocked users
    confirmedEmail: {type: Boolean, default: false}, // Confirmed Email
    confirmEmail: {type: Object, default: { }}, // confirmEmail
    verified: {type: Boolean, default: false}, // verified
    theme: {type: Number, default: 0}, // theme { 0 = "light", 1 = "dark" }
    language: {type: String, default: "de"}, // language
    level: {type: Number, default: 1}, // Level
    points: {type: Number, default: 0}, // Points
    connectedSocials: {type: Array, default: new Array()}, // connectedSocials
});

/* function to encrypt the password */
userSchema.methods.encryptPassword = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(5), null);
}

/* function to check if the password is valid */
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
}

module.exports = mongoose.model('User', userSchema);
