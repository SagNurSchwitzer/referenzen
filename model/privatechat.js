var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var data = require('../config/data');

var postSchema = new Schema({
    _id: {type: String, required: true},
    user1: {type: String, required: true},
    user2: {type: String, required: true},
    messages: {type: Array, required: true},
});

module.exports = mongoose.model('PrivateChat', postSchema);
