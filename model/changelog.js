var mongoose = require('mongoose');
var data = require('../config/data');
var Schema = mongoose.Schema;

var schema = new Schema({
    _id: {type: String, default: data.getNewUUID()},
    content: {type: String, required: true},
    date: {type: String, default: data.getDate()},
    createdAt: {type: Number, default: Date.now()}
});

module.exports = mongoose.model('Changelog', schema);
