var express = require('express');
var router = express.Router();
var data = require('../config/data');
var passport = require('passport');
var User = require('../model/user');
var Post = require('../model/post');
var email = require('../email/email');
var rateLimit = require("express-rate-limit");
var rememberMe = require('../config/rememberme');

// Register rate limit
const registerLimit = rateLimit({
    windowMs: 15 * 60 * 1000, // 15 minutes
    max: 4,  // limit each IP to 1 requests per windowMs
  });

/* GET home */
router.get('/', isLoggedIn, function(req, res, next) {

  res.redirect('/');
});

/* POST register */
router.post('/register', registerLimit, isNotLoggedIn, passport.authenticate('local-signup', {
    successRedirect: '/',
    failureRedirect: '/',
    failureFlash: true
}), (req, res) => {
    var redirect = req.query.redirect;
    if(redirect) {
        res.redirect(redirect);
    } else {
        res.redirect('/');
    }
});

router.post('/login', isNotLoggedIn, passport.authenticate('local-login', { failureRedirect: '/', failureFlash: true}), (req, res, next) => {
    res.cookie('userid', req.user._id, { maxAge: 1000 * 60 * 60 * 24 * 30});

    rememberMe.registerRememberMeToken(req.user._id, (err, token) => {
        if(err) console.error(err);

        res.cookie('remember_me', token, { maxAge: 604800000 });
    });

    var redirect = req.query.redirect;
    if(redirect) {
        res.redirect(redirect);
    } else {
        res.redirect('/');
    }
});

/* GET logout */
router.get('/logout', isLoggedIn, (req, res, next) => {
    req.logout();
    res.clearCookie('remember_me');
    res.redirect('/');
});

/* GET profile */
router.get('/profile/:name', isLoggedIn, (req, res, next) => {
    var name = req.params.name; // username from url
    var user2 = req.user.toObject();

    if(user2.theme == 0) {
        user2.dark = false;
      } else {
        user2.dark = true;
      }

    if(!name) // check if name isn't null
        return;

    var notificationlength = 0;

    for(let i = 0; i < req.user.notifications.length; i++) {
        if(req.user.notifications[i].seen == false) {
            notificationlength++;
        }
    }

    User.findOne({'username': name}, (err, user) => { // Search for users in the database
        if(err) // check for errors
            throw err;

        if(user) { // check if the user object isn't null
            isFollowed = false; // set isFollowed var

            for(let i = 0; i < req.user.followed.length; i++) {
                if(req.user.followed[i].username == name) {
                    isFollowed = true;
                }
            }

            var userFeed; // feed
            Post.find({$or: [{'creator._id': user._id }]}).sort({'createNumber': -1}).limit(20).exec(function(err, posts) { // search for posts
                if(posts)  {
                    for(let i = 0; i < posts.length; i++) { // go through the users
                        posts[i].isLiked = false; // set like var
                        if(posts[i].likes.includes(req.user._id)) { // check if likes contains the user
                            posts[i].isLiked = true; // set like var
                            posts[i].toObject().isLiked = true; // set like var
                        }
                
                        posts[i].replielength = posts[i].replies.length; // set replielength
                        posts[i].toObject().replielength = posts[i].replies.length; // set replielength
                        posts[i].replies = JSON.stringify(posts[i].replies); // set replies
                        posts[i].toObject().replies = JSON.stringify(posts[i].replies); // set replies
                      }
                    res.render('user/profile', { notificationlength: notificationlength, feed: posts, isFollowed: isFollowed, isOwnPage: name == req.user.username, followed: req.user.followed, user: user2, target: user, isLoggedIn: true, title: data.title}); // render the page
                } else {
                    res.render('user/profile', { notificationlength: notificationlength, feed: userFeed, isFollowed: isFollowed, isOwnPage: name == req.user.username, followed: req.user.followed, user: user2, target: user, isLoggedIn: true, title: data.title}); // render the page
                }
            });
        } else {
            User.findOne({'_id': name}, (err, user1) => {
                if(err) console.error(err);

                if(user1) {
                    isFollowed = false; // set isFollowed var

                    for(let i = 0; i < req.user.followed.length; i++) {
                        if(req.user.followed[i].username == name) {
                            isFollowed = true;
                        }
                    }

                    var userFeed; // feed
                    Post.find({$or: [{'creator._id': user1._id }]}).sort({'createNumber': -1}).limit(20).exec(function(err, posts) { // search for posts
                        if(posts)  {
                            for(let i = 0; i < posts.length; i++) { // go through the users
                                posts[i].isLiked = false; // set like var
                                if(posts[i].likes.includes(req.user._id)) { // check if likes contains the user
                                    posts[i].isLiked = true; // set like var
                                    posts[i].toObject().isLiked = true; // set like var
                                }
                        
                                posts[i].replielength = posts[i].replies.length; // set replielength
                                posts[i].toObject().replielength = posts[i].replies.length; // set replielength
                                posts[i].replies = JSON.stringify(posts[i].replies); // set replies
                                posts[i].toObject().replies = JSON.stringify(posts[i].replies); // set replies
                            }
                            res.render('user/profile', { notificationlength: notificationlength, feed: posts, isFollowed: isFollowed, isOwnPage: name == req.user.username, followed: req.user.followed, user: user2, target: user1, isLoggedIn: true, title: data.title}); // render the page
                        } else {
                            res.render('user/profile', { notificationlength: notificationlength, feed: userFeed, isFollowed: isFollowed, isOwnPage: name == req.user.username, followed: req.user.followed, user: user2, target: user1, isLoggedIn: true, title: data.title}); // render the page
                        }
                    });
                } else {
                    req.flash('error', 'Es existiert kein Nutzer unter dem Benutzernamen "' + name + '"!'); // set error message
                    res.redirect('/'); // redirect to the homepage
                }
            }) 
        }
    });
});

/* GET settings */
router.get('/settings', isLoggedIn, (req, res, next) => {
    var user = req.user.toObject();
    var notificationlength = 0;

    if(user.theme == 0) {
        user.dark = false;
      } else {
        user.dark = true;
      }

     for(let i = 0; i < req.user.notifications.length; i++) {
         if(req.user.notifications[i].seen == false) {
             notificationlength++;
         }
     }


    res.render('user/settings', { notificationlength: notificationlength, title: data.title, followed: req.user.followed, user: user, isLoggedIn: true }); // render page
});

/* GET email */
router.get('/settings/email', isLoggedIn, (req, res, next) => {
    var user = req.user.toObject();
    var notificationlength = 0;

    if(user.theme == 0) {
        user.dark = false;
      } else {
        user.dark = true;
      }

     for(let i = 0; i < req.user.notifications.length; i++) {
         if(req.user.notifications[i].seen == false) {
             notificationlength++;
         }
     }

    res.render('user/email', { notificationlength: notificationlength, user: user, title: data.title, isLoggedIn: true}); // render page
});

/* GET password */
router.get('/settings/password', isLoggedIn, (req, res, next) => {
    var user = req.user.toObject();
    var notificationlength = 0;

    if(user.theme == 0) {
        user.dark = false;
      } else {
        user.dark = true;
      }

     for(let i = 0; i < req.user.notifications.length; i++) {
         if(req.user.notifications[i].seen == false) {
             notificationlength++;
         }
     }

    res.render('user/password', { notificationlength: notificationlength, user: user, title: data.title, isLoggedIn: true}); // render page
});

/* GET location */
router.get('/settings/location', isLoggedIn, (req, res, next) => {
    var user = req.user.toObject();
    var notificationlength = 0;

    if(user.theme == 0) {
        user.dark = false;
      } else {
        user.dark = true;
      }

     for(let i = 0; i < req.user.notifications.length; i++) {
         if(req.user.notifications[i].seen == false) {
             notificationlength++;
         }
     }

    res.render('user/location', { notificationlength: notificationlength, user: user, title: data.title, isLoggedIn: true}); // render page
});

/* POST email */
router.post('/settings/email', isLoggedIn, (req, res, next) => {
    if(req.body.email && req.body.password) { // check for validation
        User.findOne({'_id': req.user._id}, (err, user) => { // search for user
            if(user.validPassword(req.body.password)) { // check password validation
                user.email = req.body.email; // set new email
                user.update({'email': user.email}, (err) => { if(err) throw err; }); // update user
                // send response
                res.json({
                    state: "reload"
                });
            } else {
                // send error response
                res.json({
                    state: "error",
                    message: "Falsches Passwort! Versuche es erneut!"
                })
            }
        });
    } else {
        // send error response
        res.json({
            type: "cancel 1",
            email: req.user.email
        })
    }
});

/* Post password */
router.post('/settings/password', isLoggedIn, (req, res, next) => {
    if(req.body.newPassword && req.body.oldPassword) { // check for validation
        User.findOne({'_id': req.user._id}, (err, user) => { // search for the user
            if(err) console.error(err); // send error
            
            if(user) {
                if(user.validPassword(req.body.oldPassword)) { // check for password validation
                    var Confirmation = getPasswordConfirmation(); // get new UID

                    // send email
                    email.sendMail({
                        from: 'WoidBook Email Confirmation 1@gmail.com',
                        to: req.user.email,
                        subject: 'Passwort ändern!',
                        html: '<center><h2 style="color: green;">WoidBook</h2></center><br>Klicken Sie auf den folgenden Link um Ihr Passwort zu ändern! \n ' + Confirmation
                    });

                    // update user
                    user.update({'confirmEmail': { type: "password", done: false, link: Confirmation, newPassword: req.body.newPassword }}, (err) => { if(err) console.error(err); });
                    
                    // send response
                    res.json({
                        state: "error",
                        message: "Sie haben eine Email erhalten!"
                    })
                } else {
                    // send error response
                    res.json({
                        state: "error",
                        message: "Falsches Passwort! Versuche es erneut!"
                    });
                }
            }
        })
    } else {
        // set error response
        res.json({
            type: "Missing crediantials"
        })
    }
});

/* POST location */
router.post('/settings/location', isLoggedIn, (req, res, next) => {
    if(req.body.country || req.body.town) { // check for validation
        User.findOne({'_id': req.user._id}, (err, user) => { // search for user
            if(err) console.error(err); // send error

            if(user) {
                if(req.body.country) user.address.country = req.body.country; // set country
                if(req.body.town) user.address.town = req.body.town; // set town

                user.update({'address': user.address}, (err) => { if(err) console.error(err); }); // update user

                // send response
                res.json({
                    state: "reload"
                });
            }
        });
    } else {
        // send error response
        res.json({
            type: "Missing crediantials"
        })
    }
})

function registerActivity(req, res, next) {
    /*
    User.findOne({'_id': req.user._id}, (err, user) => {
        if(err)
            throw err;

        if(user)
            user.update({'lastOnline': data.getDate()}, (err) => { return next(); });
    });
    */
}

// function to get a new passwor uid
function getPasswordConfirmation() {
    var url = "https://woidbook.com/password/" + data.getNewUUID();
    return url;
}

// function to check if the user is logged in
function isLoggedIn(req, res, next) {
    if(req.isAuthenticated()) {
        return next();
    }
    res.redirect("/?redirect=" + req.originalUrl);
}


// function to check if the user isn't logged in
function isNotLoggedIn(req, res, next) {
    if(!req.isAuthenticated()) {
        return next();
    }
    res.redirect('/');
}

module.exports = router;
