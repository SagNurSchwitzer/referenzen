var express = require('express');
var router = express.Router();
var data = require('../config/data');
var User = require('../model/user');

/* GET search page */
router.get('/*', isLoggedIn, (req, res, next) => {
    var user = req.user.toObject();
    var notificationlength = 0;

    if(user.theme == 0) {
        user.dark = false;
      } else {
        user.dark = true;
      }

    for(let i = 0; i < req.user.notifications.length; i++) {
        if(req.user.notifications[i].seen == false) {
            notificationlength++;
        }
    }

      if(req.query.search) { // check for validation
            var search = req.query.search; // var for the search string

            User.find({ "username" : {$regex: new RegExp(search), $options: "i"}, "name": {$regex: new RegExp(search), $options: "i"}}).limit(30).exec((err, users) => { // search in user database with the given keywords
                if(err) console.error(err); // log error

                if(users.length > 0) { // check for users
                    var isSearchable = true;
                    for(let i = 0; i < users.length; i++) { // loop through users
                        var userobj = users[i].toObject();
                        if(users[i]._id == req.user._id) { // check if its the same
                            users.splice(i, 1); // remove it from array
                        }
                        if(users.length > 0) {
                            for(let x = 0; x < users[i].follower.length; x++) { // loop through the follower of a user
                                if(users[i].follower[x]._id == req.user._id) { // check if the follower is the user
                                    userobj.isFollowed = true; // set isFollowed to true
                                    users[i] = userobj;
                                }
                            }
                        } else {
                            res.render('user/userEvent', { user: user, notificationlength: notificationlength, title: data.title, userTitle: "Keine Ergebnisse", message: "Unter dem Stichwort " + search + " konnten leider kein Ergebniss gefunden werden. Vielleicht haben Sie sich ja vertippt!"}); // render page
                        }
                    }

                    res.render('user/search', { notificationlength: notificationlength, title: data.title, searchTitle: search, user: user, foundedObjects: users}); // render search page
                } else {
                    res.render('user/userEvent', { user: user, notificationlength: notificationlength, title: data.title, userTitle: "Keine Ergebnisse", message: "Unter dem Stichwort " + search + " konnten leider kein Ergebniss gefunden werden. Vielleicht haben Sie sich ja vertippt!"}); // render page
                }
            });
      }
});


// function to that the user is logged in
function isLoggedIn(req, res, next) {
    if(req.isAuthenticated()) {
        next();
    } else {
        req.flash('error', "Sie müssen eingeloggt sein!");
        res.redirect("/?redirect=" + req.originalUrl);
    }
}

module.exports = router;
