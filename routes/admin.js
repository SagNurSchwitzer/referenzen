var express = require('express');
var router = express.Router();
var data = require('../config/data');
var Post = require('../model/post');
var User = require('../model/user');

/* GET home page. */
router.get('/', isLoggedIn, isAdmin, function(req, res, next) {
    var user = req.user.toObject();
    var users = 0;
    var posts = 0;
    var advertiser = 0;
    var revenue = 0;

    var notificationlength = 0;

    for(let i = 0; i < req.user.notifications.length; i++) {
        if(req.user.notifications[i].seen == false) {
            notificationlength++;
        }
    }

    if(user.theme == 0) {
        user.dark = false;
      } else {
        user.dark = true;
      }

    User.find({}, (err, userss) => {
        if(err) console.error(err);

        if(userss) {
            users = userss.length;

            Post.find({}, (err, postss) => {
                if(err) console.error(err);
        
                if(postss) {
                    posts = postss.length;
                    
                    res.render('admin/admin', { notificationlength: notificationlength, title: data.title, user: user, users: users, posts: posts, advertiser: advertiser, revenue: revenue });
                }
            });
        }
    });
});

function isLoggedIn(req, res, next) {
    if(req.isAuthenticated()) {
        next();
    } else {
        res.redirect("/redirect=" + req.originalUrl);
    }
}

function isAdmin(req, res, next) {
    if(req.user.role == "admin") {
        next();
    } else {
        res.redirect('/');
    }
}

module.exports = router;
