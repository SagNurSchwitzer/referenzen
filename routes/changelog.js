var express = require('express');
var data = require('../config/data');
var Changelog = require('../model/changelog');
var router = express.Router();

/* GET home page. */
router.post('/add', function(req, res, next) {
    if(req.body.changelog) {
        new Changelog({
            _id: data.getNewUUID(),
            content: req.body.changelog,
            date: data.getDate(),
            createdAt: Date.now()
        }).save((err) => { if(err) throw err; });

    }
    res.redirect('/');
});


router.get('/', (req, res, next) => {
    Changelog.find({}).sort({createdAt: -1}).exec((err, logs) => {
        if(err) console.error(err);
        
        var user = req.user.toObject();

        if(user.theme == 0) {
            user.dark = false;
          } else {
            user.dark = true;
          }
        res.render('changelog/changelog', { user: user, title: data.title, logs: logs, isLoggedIn: false, isNotLoggedIn: true, isEmpty: logs.length == 0});
    });
});

module.exports = router;
