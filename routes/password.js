var express = require('express');
var router = express.Router();
var data = require('../config/data');
var User = require('../model/user');

/* GET :id page */
router.get('/:id', isLoggedIn, (req, res, next) => {
    var id = req.params.id; // ID from url

    var notificationlength = 0;

    for(let i = 0; i < req.user.notifications.length; i++) {
        if(req.user.notifications[i].seen == false) {
            notificationlength++;
        }
    }

    var user = req.user.toObject();

    if(user.theme == 0) {
        user.dark = false;
      } else {
        user.dark = true;
      }

    if(id) {
        User.find({'confirmEmail.type': "password"}, (err, users) => { // Search for the user
            for(let i = 0; i < users.length; i++) { // Go through the array of users
                if(users[i].confirmEmail.link.split('/')[4] == id) { // check if the ID from URL is the same as the ID from the user
                    users[i].password = users[i].encryptPassword(users[i].confirmEmail.newPassword); // set new password
                    users[i].confirmEmail = { type: "null" }; // set confirmEmail field to null
                    users[i].update({ "$set": { "password": users[i].password, confirmEmail: users[i].confirmEmail }}, (err) => { if(err) console.error(err)}); // update the user

                    req.logout(); // log the user out

                    res.render('user/userEvent', { user: user, notificationlength: notificationlength, title: data.title, userTitle: "Passwort geändert!", message: "Dein Passwort wurde erfolgreich geändert logge dich nun bitte neu ein!"}); // render the page for the user
                }
            }
        })
    }
});


// function to that the user is logged in
function isLoggedIn(req, res, next) {
    if(req.isAuthenticated()) {
        next();
    } else {
        req.flash('error', "Sie müssen eingeloggt sein um Ihr Passwort ändern zu können!");
        res.redirect("/redirect=" + req.originalUrl);
    }
}

module.exports = router;
