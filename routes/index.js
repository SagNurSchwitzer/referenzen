var express = require('express');
var router = express.Router();
var rememberMe = require('../config/rememberme');
var data = require('../config/data');
var Post = require('../model/post');
var Todo = require('../model/todo');
var User = require('../model/user');

/* GET home page. */
router.get('/', rememberMe.rememberMe, function(req, res, next) {
  var isLoggedIn = false; // isLoggedIn condition
  var errors = req.flash('error'); // errors
  var publicClosed = false;

  if(!publicClosed) {
    if(req.isAuthenticated()) { // check if the user is logged in
      var user = req.user.toObject(); // user object from request
      isLoggedIn = true; // set the isLoggedIn condition to true
      var regex = [];
      for(let i = 0; i < user.followed.length; i++) {
        regex.push(user.followed[i]._id);
      }

      var notificationlength = 0;

      for(let i = 0; i < user.notifications.length; i++) {
          if(user.notifications[i].seen == false) {
              notificationlength++;
          }
      }

      if(user.theme == 0) {
        user.dark = false;
      } else {
        user.dark = true;
      }
  
      Post.find({ $or: [ { 'creator._id': { $in: regex }}, { 'creator._id': user._id} ]}).sort({'createNumber': -1}).limit(30).exec((err, posts) => {
          if(err) console.error(err);
  
          if(posts) {
            for(let i = 0; i < posts.length; i++) { // go through the feed
              posts[i].isLiked = false; // set liked
              if(posts[i].likes.includes(user._id)) { // check for inlcude
                posts[i].isLiked = true; // set liked
                posts[i].toObject().isLiked = true; // setz liked
              }
    
              posts[i].replielength = posts[i].replies.length; // set replielength
              posts[i].toObject().replielength = posts[i].replies.length; // set replielength
              posts[i].replies = JSON.stringify(posts[i].replies); // set replies
              posts[i].toObject().replies = JSON.stringify(posts[i].replies); // set replies
            }
    
            if(req.query.redirect) {
              res.redirect(req.query.redirect);
            } else {
              res.render('index', { redirect: req.query.redirect, notificationlength: notificationlength, followed: user.followed, user: user, hasError: errors.length > 0, error: errors, title: data.title, feed: posts, isLoggedIn: isLoggedIn,  isNotLoggedIn: isLoggedIn == false}); // render page
            }
          }
      });      
    } else {
      res.render('index', { redirect: req.query.redirect, hasError: errors.length > 0, error: errors, title: data.title, isLoggedIn: isLoggedIn,  isNotLoggedIn: isLoggedIn == false}); // render page
    }
  } else {
    res.render('info/closed', { title: data.title, isLoggedIn: false,  isNotLoggedIn: true});
  }
});

router.get('/info/datenschutz', (req, res, next) => {
  var isLoggedIn = false;
  if(req.isAuthenticated()) {
    isLoggedIn = true;

    var notificationlength = 0;

    for(let i = 0; i < req.user.notifications.length; i++) {
        if(req.user.notifications[i].seen == false) {
            notificationlength++;
        }
    }
  }

  res.render('info/datenschutz', { notificationlength: notificationlength, title: data.title, isLoggedIn: isLoggedIn });
});

router.get('/info/impressum', (req, res, next) => {
  var isLoggedIn = false;
  if(req.isAuthenticated()) {
    isLoggedIn = true;

    var notificationlength = 0;

    for(let i = 0; i < req.user.notifications.length; i++) {
        if(req.user.notifications[i].seen == false) {
            notificationlength++;
        }
    }
  }

  res.render('info/impressum', { notificationlength: notificationlength, title: data.title, isLoggedIn: isLoggedIn });
});

router.get('/about', (req, res, next) => {
  var isLoggedIn = false;
  if(req.isAuthenticated()) {
    isLoggedIn = true;

    var notificationlength = 0;

    for(let i = 0; i < req.user.notifications.length; i++) {
        if(req.user.notifications[i].seen == false) {
            notificationlength++;
        }
    }
  }

  res.render('welcome/welcome', { notificationlength: notificationlength, footer: true, title: data.title, isLoggedIn: isLoggedIn });
});

router.get('/todo', isLoggedIn, (req, res, next) => {
  Todo.find({}, (err, todos) => {
    var notificationlength = 0;

    for(let i = 0; i < req.user.notifications.length; i++) {
        if(req.user.notifications[i].seen == false) {
            notificationlength++;
        }
    }
    res.render('todo/todo', { notificationlength: notificationlength, todos: todos, user: req.user, isLoggedIn: true, isNotLoggedIn: false});
  })
})

function isLoggedIn(req, res, next) {
  if(req.isAuthenticated()) {
    next();
  } else {
    res.redirect('/');
  }
}

module.exports = router;
