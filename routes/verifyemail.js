var express = require('express');
var router = express.Router();
var data = require('../config/data');
var User = require('../model/user');
var Email = require('../email/email');

/* GET :id page */
router.get('/:id/:email', (req, res, next) => {
    var id = req.params.id; // ID from url
    var email = decodeURIComponent(req.params.email); // Email from url
    var user = null;

    if(req.isAuthenticated()) {
        user = req.user.toObject();

        if(user.theme == 0) {
            user.dark = false;
        } else {
            user.dark = true;
        }

        var notificationlength = 0;
        for(let i = 0; i < req.user.notifications.length; i++) {
            if(req.user.notifications[i].seen == false) {
                notificationlength++;
            }
        }
    }

    if(id) {
        User.find({'confirmEmail.type': "email"}, (err, users) => { // Search for the user
            if(err) res.json(err);
            var found = false;
            for(let i = 0; i < users.length; i++) { // Go through the array of users
                if(users[i].confirmEmail.link.split('/')[4] == id && users[i].confirmEmail.email == email) { // check if the ID from URL is the same as the ID from the user
                    if(Date.now() <= users[i].confirmEmail.expires) {
                        found = true;
                        users[i].confirmEmail = { type: "null" }; // set confirmEmail field to null
                        users[i].update({ "$set": { "confirmedEmail": true, "confirmEmail": users[i].confirmEmail }}, (err) => { if(err) console.error(err)}); // update the user

                        res.render('user/userEvent', { user: user, notificationlength: notificationlength, title: data.title, userTitle: "Email bestätigt!", message: "Deine Email wurde erfolgreich bestätigt! Viel Spaß beim nutzen von WoidBook.com!"}); // render the page for the user
                    } else {
                        var userEmailToken = data.getNewUUID();
                        var verificationURL = "https://woidbook.com/emailverify/" + userEmailToken + "/" + encodeURIComponent(email);

                        Email.sendMail({
                            from: "WoidBook Email Confirmation 1@gmail.com",
                            to: email,
                            subject: "Bitte bestätigen Sie Ihre Email!",
                            html: '<center><h2 style="color: green;">WoidBook</h2></center><br>Klicken Sie auf den folgenden Link um Ihre Email zu bestätigen! \n ' + verificationURL
                        });

                        users[i].confirmEmail = {
                            type: "email",
                            done: false,
                            link: verificationURL,
                            email: email,
                            expires: (Date.now() + 18000000)
                        }

                        users[i].update({'confirmEmail': users[i].confirmEmail}, (err) => {
                            if(err) console.error(err); 
                        });

                        found = true;

                        res.render('user/userEvent', { user: user, notificationlength: notificationlength, title: data.title, userTitle: "Emailcode abgelaufen!", message: "Der in der URL angegebene Emailcode ist abgelaufen! Es wurde eine neue Email an Sie gesendet!"}); // render the page for the user  
                    }
                }
            }

            if(!found) {
                res.render('user/userEvent', { notificationlength: notificationlength, title: data.title, userTitle: "Ungültiger Emailcode", message: "Der in der URL angegebene Emailcode ist falsch oder abgelaufen!"}); // render the page for the user  
            }
        })
    } else {
        res.render('user/userEvent', { notificationlength: notificationlength, title: data.title, userTitle: "Kein Emailcode!", message: "Es konnte kein Emailcode in der URL gefunden werden! Bitte versuchen sie es erneut!"}); // render the page for the user  
    }
});


// function to that the user is logged in
function isLoggedIn(req, res, next) {
    if(req.isAuthenticated()) {
        next();
    } else {
        req.flash('error', "Sie müssen eingeloggt sein um Ihr Passwort ändern zu können!");
        res.redirect("/redirect=" + req.originalUrl);
    }
}

module.exports = router;
