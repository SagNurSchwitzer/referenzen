var express = require('express');
var router = express.Router();
var data = require('../config/data');
var jwt = require('jsonwebtoken');
var decodeBearer = require('../api/decodeBearer');
var Post = require('../model/post');
var User = require('../model/user');


/*

    FIREBASE

*/

var admin = require('firebase-admin');
var serviceAccount = require('../config/firebaseAdmin.json');

// Initialize Firebase Admin SDK
admin.initializeApp({
  serviceAccountId: "firebase-adminsdk-jepwq@woidbook.iam.gserviceaccount.com",
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://woidbook.firebaseio.com"
});


/*

    USER

*/

router.get('/isAuthenticated', (req, res) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token, data.secret);
        res.status(200).json({
            type: "success",
            message: "The auth isn't expired!",
            data: decoded
        })
    } catch (error) {
        return res.status(401).json({
            type: "error",
            error: {
                status: 401,
                reason: "notloggedin",
                message: "You are not logged in!"
            }
        });
    }
});

/* POST Authenticate */
router.post('/authenticate', (req, res) => {
    if(req.body.email && req.body.password) {
        User.findOne({'email': req.body.email}, (err, user) => {
            if(err) console.error(err);

            if(user) {
                if(user.validPassword(req.body.password)) {
                    // User information
                    const payload = {
                        _id: user._id,
                        username: user.username,
                        name: user.name,
                        email: user.email,
                        description: user.description,
                        blocked: user.blocked,
                        theme: user.theme
                    };

                    // Create JWT token
                    var token = jwt.sign(payload, data.secret, {
                        expiresIn: "24h"
                    });
                
                    // Create Firebase Custom Token
                    admin.auth().createCustomToken(user._id, payload)
                        .then((customToken) => {
                            res.status(200).json({
                                type: "success",
                                message: "The authentication was successful",
                                firebaseToken: customToken,
                                token: token,
                                user: user
                            });
                        })
                        .catch((err) => {
                            if(err) console.error(err);
                        });
                } else {
                    res.status(403).json({
                        type: "error",
                        error: {
                            status: 403,
                            reason: "password",
                            message: "The password isn't right"
                        }
                    });
                }
            } else {
                res.status(404).json({
                    type: "error",
                    error: {
                        status: 404,
                        reason: "nouser",
                        message: "No user could be found"
                    }
                })
            }
        });
    } else {
        res.status(404).json({
            type: "error",
            error: {
                status: 404,
                reason: "missingcredentials",
                message: "Missing credentials"
            }
        })
    }
});

// GET cached userdata
router.get('/cachedUserData', decodeBearer, (req, res, next) => {
    res.status(200).json({
        type: "success",
        data: req.decodedApiData
    });
});

// GET database userdata
router.get('/databaseUserData/:userid', decodeBearer, (req, res, next) => {
    if(req.params.userid) {
        User.findById(req.params.userid, (err, user) => {
            if(err) res.status(500).json(err);
    
            if(user) {
                res.status(200).json({
                    type: "success",
                    data: user
                });
            } else {
                User.findOne({'username': req.params.userid}, (err, user1) => {
                    if(err) res.status(500).json(err);
                    if(user1) {
                        res.status(200).json({
                            type: "success",
                            data: user1
                        });
                    } else {
                        res.status(404).json({
                            type: "error",
                            error: {
                                status: 404,
                                reason: "nouser",
                                message: "No user could be found in the database"
                            }
                        })
                    }
                });
            }
        });
    } else {
        res.status(400).json({
            type: "error",
            error: {
                status: 400,
                reason: "missingcredentials",
                message: "No userid object could be found in the path"
            }
        })
    }
});

// POST updateLocation
router.post('/updateLocation', decodeBearer, (req, res) => {
    if(req.body.country || req.body.town) { // check for validation
        User.findOne({'_id': req.decodedApiData._id}, (err, user) => { // search for user
            if(err) console.error(err); // send error

            if(user) {
                if(req.body.country) user.address.country = req.body.country; // set country
                if(req.body.town) user.address.town = req.body.town; // set town

                user.update({'address': user.address}, (err) => { if(err) console.error(err); }); // update user

                // send response
                res.status(200).json({
                    type: "success",
                    message: "Town or country is changed"
                });
            }
        });

    } else {
        // send error response
        res.status(400).json({
            type: "error",
            error: {
                status: 400,
                reason: "missingcredentials",
                message: "Missing credentials"
            }
        })
    }
});

// Post updateEmail
router.post('/updateEmail', decodeBearer, (req, res) => {
    if(req.body.email && req.body.password) { // check for validation
        User.findOne({'_id': req.user._id}, (err, user) => { // search for user
            if(user.validPassword(req.body.password)) { // check password validation
                user.email = req.body.email; // set new email
                user.update({'email': user.email}, (err) => { if(err) throw err; }); // update user
                // send response
                res.status(200).json({
                    type: "success",
                    message: "Email is changed"
                });
            } else {
                // send error response
                res.status(403).json({
                    type: "error",
                    error: {
                        status: 403,
                        reason: "password",
                        message: "Wrong password! Try again"
                    }
                })
            }
        });
    } else {
        // send error response
        res.status(400).json({
            type: "error",
            error: {
                status: 400,
                reason: "missingcredentials",
                message: "Missing credentials"
            }
        })
    }
});

// GET Notifications
router.get('/notifications', decodeBearer, (req, res, next) => {
    User.findById(req.decodedApiData._id, (err, user) => {
        if(err) res.status(500).json({type: "error", error: err});

        if(user) {
            res.status(200).json({
                type: "success",
                notifications: user.notifications,
                userid: req.decodedApiData._id
            });
        }
    })
});

/*

    FEED 

*/

// Get userFeed
router.get('/userFeed', decodeBearer, (req, res) => {
    User.findById(req.decodedApiData._id, (err, user) => {
        if(err) console.error(err);

        if(user) {
            var regex = [];
            regex.push(new RegExp(user._id));
            for(let i = 0; i < user.followed.length; i++) {
                regex.push(new RegExp(user.followed[i]._id));
            }

            Post.find({ 'creator._id': { $in: regex }}).sort({'createNumber': -1}).limit(30).exec((err, posts) => {
                if(err) console.error(err);

                if(posts) {
                    for(let i = 0; i < posts.length; i++) { // go through the feed
                        var p = posts[i].toObject();
                        p.isLiked = false; // set liked
                        if(posts[i].likes.includes(user._id)) { // check for inlcude
                            p.isLiked = true; // set liked
                        }
            
                        p.replielength = posts[i].replies.length; // set replielength
                        posts[i] = p;
                    }
        
                    res.status(200).json({
                        type: "success",
                        feed: posts,
                        user: user
                    });
                }
            }); 
        } else {
            res.status(404).json({
                type: "error",
                error: {
                    status: 404,
                    reason: "nouser",
                    message: "No user could be found in the database"
                }
            })
        }
    });
});

// Get singleUserFeed
router.get('/singleUserFeed/:userid', decodeBearer, (req, res, next) => {
    if(!req.params.userid) res.status(404).json({type: "error", error: {message: "Incomplete body"}});
    Post.find({ 'creator._id': req.params.userid}).limit(30).sort({'createNumber': -1}).exec((err, posts) => {
        if(err) console.error(err);

        if(posts) {
            for(let i = 0; i < posts.length; i++) { // go through the feed
                var p = posts[i].toObject();
                p.isLiked = false; // set liked
                if(posts[i].likes.includes(req.decodedApiData._id)) { // check for inlcude
                    p.isLiked = true; // set liked
                }
    
                p.replielength = posts[i].replies.length; // set replielength
                posts[i] = p;
            }

            res.status(200).json({
                type: "success",
                feed: posts
            }); 
        }
    });
});

/*

POST

*/

// Post createPost
router.post('/createPost', decodeBearer, (req, res, next) => {
    if(!req.body.message || !req.body.createNumber) res.status(404).json({type: "error", error: {message: "Incomplete body"}});
    var reqTime = new Date(parseInt(req.body.createNumber));
    var time = reqTime.getDate() + "." + (reqTime.getUTCMonth() + 1) + "." + reqTime.getFullYear() + ", " + reqTime.toLocaleTimeString();
    var postObject = {
        _id: config.getNewUUID(), // set ID
        creator: req.decodedApiData._id, // set the creator
        content: req.body.message, // set the message
        images: [], // set the images
        videos: [], // set the videos
        createdAt: time, // set the createdAt 
        createNumber: req.body.createNumber // set the createNumber
    }

    var files = req.files;
    var upload = true;
    if(files != null) {
        if(files.images[0]) {
            for(let i = 0; i < files.images.length; i++) {
                if((files.images[i].mimetype == "image/jpeg" || files.images[i].mimetype == "image/png")) {
                    upload = true;
                } else {
                    files.images.splice(i, 1);
                }
            }
        } else {
            if((files.images.mimetype == "image/jpeg" || files.images.mimetype == "image/png")) {
                upload = true;
            } else {
                upload = false;
            }
        }
    }

    if(files && (files.images[0] || files.images) && upload) {
        socket.uploadImage({postObject: postObject, images: JSON.stringify(files.images)});
    } else if(upload) {
        new Post(postObject)
            .save((err) => { // save the object into the database
                if(err) 
                    res.status(500).json({error: err}); // throw an error if one exists
                res.status(200).json({
                    type: "success",
                    message: "The Post was successfully created",
                    postid: postObject._id
                });
        });
    }
});

/*

    POST

*/

// Get single post
router.get('/getPost/:id', decodeBearer, (req, res, next) => {
    if(!req.params.id) res.status(404).json({type: "error", error: {message: "Incomplete body"}});

    Post.findById(req.params.id, (err, post) => {
        if(err) res.status(500).json({type: "error", error: err});

        if(post) {
            res.status(200).json({
                type: "success",
                post: post,
                message: "Successfully got post with id" + req.params.id
            });
        }
    })
});

// Followstatechange
router.post('/changeFollowState', decodeBearer, (req, res, next) => {
    if(!req.body.targetid) res.status(404).json({type: "error", error: {message: "Incomplete body"}});

    User.find({"_id": [req.body.targetid, req.decodedApiData._id]}, (err, users) => {
        if(err) res.status(500).json({type: "error", error: err});
        var user;
        var target;

        for(let i = 0; i < users.length; i++) {
            if(users[i]._id == req.decodedApiData._id) {
                user = users[i];
            } else {
                target = users[i];
            }
        }

        var followAdd = true;
        for(let i = 0; i < user.followed.length; i++) {
            if(user.followed[i]._id == target._id) {
                user.followed.splice(i, 1);
                followAdd = false;
            }
        }

        if(followAdd) {
            user.followed.push({_id: target._id, username: target.username});
            target.follower.push({_id: user._id, username: user.username});
            target.notifications.push({message: "Der Benutzer " + user.name + " folgt dir nun!", seen: false, date: Date.now()});
        } else {
            for(let i = 0; i < target.follower.length; i++) {
                if(target.follower[i]._id == user._id) {
                    target.follower.splice(i, 1);
                }
            }
        }

        user.update({"followed": user.followed}, (err) => { if(err) res.status(500).json({"type": "error", error: err})});
        target.update({"follower": target.follower, "notifications": target.notifications}, (err) => { if(err) res.status(500).json({"type": "error", error: err})});

        res.status(200).json({
            type: "success",
            message: "Successfully changed follow state",
            followed: followAdd,
        });
    })
});

// Like Post
router.post('/changeLikePost', decodeBearer, (req, res, next) => {
    if(!req.body.postid) res.status(404).json({type: "error", error: {message: "Incomplete body"}});

    Post.findById(req.body.postid, (err, post) => {
        if(err) res.status(500).json({type: "error", error: err});

        if(post) {
            var exists = false;
            for(let i = 0; i < post.likes.length; i++) {
                if(post.likes[i] == req.decodedApiData._id) {
                    exists = true;
                }
            }

            if(!exists) {
                post.likes.push(req.decodedApiData._id);
                post.update({'likes': post.likes}, (err) => { if(err) res.status(408).json({type: "error", error: err}); });
                res.status(200).json({
                    type: "success",
                    postid: req.body.postid,
                    likes: post.likes.length,
                    message: "The Post was successfully liked"
                });
            } else {
                for(let i = 0; i < post.likes.length; i++) {
                    if(post.likes[i] == req.decodedApiData._id) {
                        post.likes.splice(i, 1);
                        post.update({'likes': post.likes}, (err) => { if(err) res.status(408).json({type: "error", error: err}); });
                        res.status(200).json({
                            type: "success",
                            postid: req.body.postid,
                            likes: post.likes.length,
                            message: "The Post was successfully unliked"
                        });
                    }
                }
            }
        }
    });
});

// POST commentPost
router.post('/commentPost', decodeBearer, (req, res, next) => {
    if(!req.body.postid || !req.body.message || req.body.message.length < 1 || !req.body.time) res.status(404).json({type: "error", error: {message: "Incomplete body"}});

    Post.findById(req.body.postid, (err, post) => {
        if(err) res.status(500).json({type: "error", error: err});

        if(post) {
            var reqTime = new Date(parseInt(req.body.time));
            var time = reqTime.getDate() + "." + (reqTime.getUTCMonth() + 1) + "." + reqTime.getFullYear() + ", " + reqTime.toLocaleTimeString();
            post.replies.push({creator: {_id: req.decodedApiData._id, name: req.decodedApiData.username}, message: req.body.message, time: time});
            post.update({'replies': post.replies}, (err) => { if(err) res.status(404).json({type: "error", error: err}); });

            User.findById(post.creator._id, (err, user) => {
                if(err) console.error(err);

                if(user) {
                    var notifymessage = req.decodedApiData.username + " hat deinen Beitrag kommentiert!";
                    var contains = false;
                    for(let i = 0; i < user.notifications.length; i++) {
                        if(user.notifications[i].message == notifymessage) {
                            contains = true;
                        }
                    }
                    if(!contains) {
                        user.notifications.push({message: notifymessage, seen: false, date: Date.now()});
                        user.update({'notifications': user.notifications}, (err) => { if(err) console.error(err); });
                    }
                }
            });

            res.status(200).json({
                type: "success",
                message: "Successfully posted a comment to the post: " + req.body.postid,
                comment: req.body.message
            });
        }
    });
});

/* GET search */
router.get('/*', decodeBearer, (req, res, next) => {
    if(req.query.search) { // check for validation
          var search = req.query.search; // var for the search string

          User.find({ "username" : {$regex: new RegExp(search), $options: "i"}, "name": {$regex: new RegExp(search), $options: "i"}}).limit(30).exec((err, users) => { // search in user database with the given keywords
              if(err) console.error(err); // log error

              if(users.length > 0) { // check for users
                  console.log(users);
                  for(let i = 0; i < users.length; i++) { // loop through users
                    var userobj = users[i].toObject();
                      if(users[i]._id == req.decodedApiData._id) { // check if its the same
                          users.splice(i, 1); // remove it from array
                      }
                      if(users.length > 0) {
                          for(let x = 0; x < users[i].follower.length; x++) { // loop through the follower of a user
                              if(users[i].follower[x]._id == req.decodedApiData._id) { // check if the follower is the user
                                  userobj.isFollowed = true; // set isFollowed to true
                                  users[i] = userobj;
                              }
                          }
                      } else {
                          res.status(200).json({
                              type: "success",
                              message: "No objects could be found",
                              foundedObjects: null,
                              searchTitle: search
                          });
                      }
                  }
                  res.status(200).json({
                    type: "success",
                    message: "Objects could be found",
                    foundedObjects: users,
                    searchTitle: search
                  });
              } else {
                res.status(200).json({
                    type: "success",
                    message: "No objects could be found",
                    foundedObjects: null,
                    searchTitle: search
                });

              }
          });
    } else {
        res.status(404).json({type: "error", error: {message: "Incomplete body"}});
    }
});


module.exports = router;
