var express = require('express');
var router = express.Router();
var config = require('../config/data');
var Post = require('../model/post');
var User = require('../model/user');
var socket = require('../config/socket');
var email = require('../email/email');

/* POST create */
router.post('/create', isLoggedIn, (req, res, next) => {
    var user = req.user;
    var bodyObjects = { // object with the body values
        message: req.body.message, // post message
    }

        if(bodyObjects.message) { // check if the post message exists
            var splittedMessage = bodyObjects.message.split(' ');
            for(let i = 0; i < splittedMessage.length; i++) {
                if(splittedMessage[i].startsWith('@')) {
                    var str = splittedMessage[i];
                    str = str.replace('@', '');
                    User.findOne({'username': str}, (err, marked) => {
                        console.log(marked);
                        if(err) console.error(err);

                        if(marked) {
                            var notifyMessage = "Du wurdest von " + user.name + " in einem Beitrag makiert!";
                            var notifyContains = false;
                            for(let x = 0; x < marked.notifications.length; x++) {
                                if(marked.notifications[x].message == notifyMessage) {
                                    notifyContains = true;
                                }
                            }

                            if(!notifyContains) {
                                marked.notifications.push({message: notifyMessage, seen: false, date: Date.now()});
                                marked.update({'notifications': marked.notifications}, (err) => { if(err) console.error(err); });
                            }
                        }
                    });
                }
            }

            var files = req.files;
            console.log(files);
            var upload = true;
            if(files != null) {
                if(files.images[0]) {
                    for(let i = 0; i < files.images.length; i++) {
                        if((files.images[i].mimetype == "image/jpeg" || files.images[i].mimetype == "image/png")) {
                            upload = true;
                        } else {
                            files.images.splice(i, 1);
                        }
                    }
                } else {
                    if((files.images.mimetype == "image/jpeg" || files.images.mimetype == "image/png")) {
                        console.log(1);
                        upload = true;
                    } else {
                        console.log(3);
                        upload = false;
                    }
                }
            }

            var postObject = {
                _id: config.getNewUUID(), // set ID
                creator: user, // set the creator
                content: bodyObjects.message, // set the message
                images: [], // set the images
                videos: [], // set the videos
                createdAt: req.body.createdAt, // set the createdAt 
                createNumber: req.body.createNumber // set the createNumber
            }

            if(files && (files.images[0] || files.images) && upload) {
                console.log("upload");
                socket.uploadImage({postObject: postObject, images: JSON.stringify(files.images)});
            } else if(upload) {
                console.log(2);
                console.log(upload);
                new Post(postObject)
                    .save((err) => { // save the object into the database
                        if(err) 
                            throw err; // throw an error if one exists
                });
            }


            setTimeout(() => {
                res.status(200).json({
                    state: "reload"
                });
            }, 500);

        } else {
            req.flash('error', data.invalidBody); // set the errors
            res.status(200).json({ // response with an 200 status and json body object
                state: "reload" 
            });
        }
});

function isLoggedIn(req, res, next) {
    if(req.isAuthenticated()) {
        return next();
    }
    res.redirect('/');
}

module.exports = router;
