const Token = require('../model/token');
const User = require('../model/user');
const data = require('../config/data');

function rememberMe(req, res, next) {
    if(req.cookies.remember_me && !req.isAuthenticated()) {
        Token.findOne({'token': req.cookies.remember_me}, (err, tokenobj) => {
            if(err) console.error(err);

            if(tokenobj) {
                var userid = tokenobj.userID;
                User.findOne({'_id': userid}, (err, user) => {
                    if(err) console.error(err);

                    if(user) {
                        req.login(user, (err) => {
                            if(err) console.error(err);

                            var token = data.randomString(64);
                            tokenobj.update({'token': token}, (err) => { if(err) console.error(err)});
                            res.cookie('remember_me', token, { path: '/', maxAge: 604800000 });
                            return next();
                        });
                    }
                })
            } else {
                return next();
            }
        })
    } else {
        return next();
    }
}

function registerRememberMeToken(userid, callback) {
    var token = data.randomString(64);
    Token.findOne({'userID': userid}, (err, tokenobj) => {
        if(err) console.error(err);

        if(tokenobj) {
            tokenobj.update({'token': token}, (err) => { if(err) console.error(err); });
        } else {
            var newTokenObj = new Token();
            newTokenObj.token = token;
            newTokenObj.userID = userid;
            newTokenObj.save((err) => { if(err) console.error(err) });
        }
    });

    callback(null, token);
}

module.exports = {
    rememberMe,
    registerRememberMeToken
}
