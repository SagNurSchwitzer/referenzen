/* function to create a new UUID */
function getNewUUID() {
    function s4() { // function to create a amount of random numbers
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }
      return s4() + s4() + '-' + s4() + s4() + '-' + s4(); // return the final uuid as a string
}

/* function to get the date in the right format */
function getDate() {
    var date = new Date(); // date object

    return date.toLocaleString(); // return localstring
}

function randomString(len) {
    var buf = []
      , chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
      , charlen = chars.length;
  
    for (var i = 0; i < len; ++i) {
      buf.push(chars[getRandomInt(0, charlen - 1)]);
    }
  
    return buf.join('');
};

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

module.exports.title = 'Woidbook | Das neuste aus dem Bayerischen Wald';
module.exports.invalidBody = 'Bitte fülle alle Felder aus und versuche es erneut!';
module.exports.secret = "woidbookisbaba";
module.exports.databasepassword = "mongodb+srv://TestAdmin:test@woidbook-iwwlt.mongodb.net/test";
module.exports.getNewUUID = getNewUUID;
module.exports.getDate = getDate;
module.exports.randomString = randomString;