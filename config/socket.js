var User = require('../model/user');
var Post = require('../model/post');
var Todo = require('../model/todo')
var fs = require('fs');
var socketClient = require('socket.io-client');
var io;
var client;

/* function to setup socket.io */
function init(server) {
    io = require('socket.io')(server, {
        origins: 'http://localhost:* http://.woidbook.com:* https://woidbook.com:* http://image.woidbook.com:* https://image.woidbook.com:*'
    }); // set the http server
    client = socketClient.connect('https://image.woidbook.com', { reconnect: true });

    io.on('connection', (socket) => { // check for connections
        socket.on('changeName', (data) => { // wait for changeName event
            if(data && data.name && data._id) { // check for validation
                User.findOne({'_id': data._id}, (err, user) => { // Check for user in database
                    if(err) console.error(err); // throw error if exists

                    if(user) { // check if user exists
                        user.update({'name': data.name}, (err) => { if(err) console.error(err); }); // update user
                    }
                });
            }
        });

        socket.on('deleteAccount', (data) => {
            if(data.userid) {
                Post.find({'creator._id': data.userid}, (err, posts) => {
                    if(err) console.error(err);

                    if(posts) {
                        for(let i = 0; i < posts.length; i++) {
                            posts[i].remove((err) => {if(err) console.error(err)});
                        }
                    }
                });

                User.findById(data.userid, (err, user) => {
                    const searchObject = {
                        _id: data.userid,
                        username: user.username
                    };

                    User.find({"followed": { $in: [searchObject] }}, (err, users) => {
                        if(err) console.error(err);
                        
                        if(users) {
                            for(let i = 0; i < users.length; i++) {
                                process.nextTick(() => {
                                    for(let x = 0; x < users[i].followed.length; x++) {
                                        if(users[i].followed[x]._id == searchObject._id) {
                                            users[i].followed.splice(x, 1);
                                            users[i].update((err) => { if(err) console.error(err); });
                                        }
                                    }
                                });
                            }
                        }
                    });

                    user.remove((err) => { if(err) console.error(err); });
                });
            }
        });

        socket.on('toogleTheme', (data) => {
            if(data.theme && data._id) {
                User.findById(data._id, (err, user) => {
                    if(err) console.error(err);

                    if(user) {
                        if(data.theme == "dark") {
                            user.theme = 1;
                        } else {
                            user.theme = 0;
                        }

                        user.update({"theme": user.theme}, (err) => { if(err) console.error(err); });
                        socket.emit('reload', { reason: "just reload"});
                    }
                })
            }
        })

        socket.on('changeBirthday', (data) => { // for changeDescription event
            if(data && data.birthday && data._id) { // check for validation
                User.findById(data._id, (err, user) => { // Find user in database
                    if(err) console.error(err); // throw error if exists

                    if(user) { // check if user exists
                        user.update({'birthday': data.birthday}, (err) => { if(err) console.error(err); }); // update user
                    }
                });
            }
        });

        socket.on('changeDescription', (data) => { // for changeDescription event
            if(data && data.description && data._id) { // check for validation
                User.findById(data._id, (err, user) => { // Find user in database
                    if(err) console.error(err); // throw error if exists

                    if(user) { // check if user exists
                        user.update({'description': data.description}, (err) => { if(err) console.error(err); }); // update user
                    }
                });
            }
        })

        socket.on('notifications', (data) => { // wait for notification event
            if(data.userid) { // check for validation
                var userid = data.userid; // set userid variable

                User.findOne({'_id': userid}, (err, user) => { // search for user
                    if(err) console.error(err); // log error

                    if(user) { // check user
                        if(user.notifications) { // check notifications
                            for(let i = 0; i < user.notifications.length; i++) { // loop through the notifications
                                if(user.notifications[i].seen == false) { // check if the seen field is on false
                                    user.notifications[i].seen = true; // set seen to true
                                }
                            }
                            user.update({'notifications': user.notifications}, (err) => { if(err) console.log(err); }); // update notifications field
                        }
                    }
                });
            }
        });

        socket.on('follow', (data) => { // wait for follow event
            var userid = data.userid; // get the userid
            var followedid = data.followedid; // get the id of the followed person

            if(userid && followedid) { // check if both aren't null
                if(userid != followedid) { // check for non equal
                    follow(userid, followedid); // execute the follow function
                }
            }
        });

        socket.on('unfollow', (data) => { // wait for follow event
            var userid = data.userid; // get the userid
            var followedid = data.followedid; // get the id of the followed person

            User.find({'_id': [userid, followedid]}, (err, users) => { // search for the users in the database
                if(err) console.error(err); // throw err
    
                if(users) { // check if something was found in the database
                    var user;
                    var followed;
    
                    // set the user objects
                    if(users[0]._id == userid) {
                        user = users[0];
                        followed = users[1];
                    } else {
                        user = users[1];
                        followed = users[0];
                    }
    
                        // Get follower arrays an add the new user to it
                        var followeda = followed.toObject().follower;
                        for(let i = 0; i < followeda.length; i++) {
                            if(followeda[i]._id == user._id) {
                                followeda.splice(i, 1);
                            }
                        }
                        var usera = user.toObject().followed;
                        for(let i = 0; i < usera.length; i++) {
                            if(usera[i]._id == followed._id) {
                                usera.splice(i, 1);
                            }
                        }
    
                        // update user objects
                        process.nextTick(() => { // async
                            user.update({'followed': usera}, (err) => { // update first user
                                process.nextTick(() => { // async
                                    followed.update({'follower': followeda}, (err) => { }); // update second user
                                });
                            });
                        });
                    
                }
            });
        });

        socket.on('setUserProfileImage', (data) => { // wait for setUserProfileEvent
            if(data) { // check for data
                User.findOne({'_id': data.userid}, (err, user) => { // search for iuser
                    if(err) console.error(err); // throw error if exists
    
                    if(user) { // check if user exists
                        user.image = data.path; // set image path to image server path
                        user.update({'image': user.image}, (err) => { if(err) throw err; }); // update user
                    }
                });
            }
        });

        socket.on('likePost', (data) => { // wait for likePost event
            if(data) { // check for data
                Post.findOne({'_id': data.postObject}, (err, post) => { // search user in database
                    if(err) console.error(err); // throw error if exists

                    if(post) { // check if post exists
                        var exists = false; // exists variable
                        for(let i = 0; i <  post.likes.length; i++) { // set loop
                            if(post.likes[i] == data.userid) { // check if postlikes contains the user
                                exists = true; // set exists = true
                            }
                        }

                        if(exists == false) { // if exists false
                            User.findOne({'_id': data.userid}, (err, user) => { // search for user in database
                                if(user) { // check for user
                                    post.likes.push(user._id); // add userid to like array
                                    post.update({'likes': post.likes}, (err) => {if(err) throw err;}); // update post object
                                    socket.emit('reload', {reason: 'like done'}); // response with reload event
                                }
                            });
                        }
                    }
                })
            }
        });

        socket.on('unlikePost', (data) => {
            if(data) {
                Post.findOne({'_id': data.postObject}, (err, post) => {
                    if(err) throw err;

                    if(post) {
                        for(let i = 0; i <  post.likes.length; i++) {
                            if(post.likes[i] == data.userid) {
                                post.likes.splice(i, 1);
                                post.update({'likes': post.likes}, (err) => {if(err) throw err;});
                                socket.emit('reload', {reason: 'like done'});
                            }
                        }
                    }
                });
            }
        });

        socket.on('getCommentInfo', (data) => {
            if(data && data._id) {
                Post.findById(data._id, (err, post) => {
                    if(err) console.error(err);

                    if(post) {
                        socket.emit('commentInfo', { type: "success", comment: post });
                    }
                });
            }
        })

        socket.on('commentPost', (data) => {
            if(data && data.postObject) {
                Post.findOne({'_id': data.postObject}, (err, post) => {
                    if(err) throw err;

                    if(post) {
                        post.replies.push({creator: {_id: data._id, name: data.username}, message: data.message, time: data.time});
                        post.update({'replies': post.replies}, (err) => {if(err) throw err;});

                        User.findById(post.creator._id, (err, user) => {
                            if(err) console.error(err);

                            if(user) {
                                var notifymessage = data.username + " hat deinen Beitrag kommentiert!";
                                var contains = false;
                                for(let i = 0; i < user.notifications.length; i++) {
                                    if(user.notifications[i].message == notifymessage) {
                                        contains = true;
                                    }
                                }
                                if(!contains) {
                                    user.notifications.push({message: notifymessage, seen: false, date: Date.now()});
                                    user.update({'notifications': user.notifications}, (err) => { if(err) console.error(err); });
                                }
                            }
                        });
                        socket.emit('reload', {reason: 'reply done'});
                    }
                });
            }
        });

        socket.on("todoDone", (data) => {
            if(data.todoid) {
                Todo.findById(data.todoid, (err, todo) => {
                    if(err) console.error(err);

                    if(todo) {
                        todo.update({'done': true}, (err) => { if(err) console.error(err); });
                        socket.emit('reload', {reason: 'reply done'});
                    }
                });
            }
        }); 
        
        socket.on("todoDelete", (data) => {
            if(data.todoid) {
                Todo.findByIdAndDelete(data.todoid, (err) => {
                    if(err) console.error(err);
                    socket.emit('reload', {reason: 'reply done'});
                });
            }
        });
    });

    client.on('post-imagedone-ready', (data) => {
        if(data && data.state == "success") {
            var postObject = data.postObjects;

            postObject.images = data.paths;

            new Post(postObject)
                .save((err) => { if(err) throw err; }); // save post
        }
    });
}

function follow(userid, followedid) { // function to follow a user
    if(userid && followedid) { // check if both aren't null
        User.find({'_id': [userid, followedid]}, (err, users) => { // search for the users in the database
            if(err) throw err; // throw err

            if(users) { // check if something was found in the database
                var user;
                var followed;

                // set the user objects
                if(users[0]._id == userid) {
                    user = users[0];
                    followed = users[1];
                } else {
                    user = users[1];
                    followed = users[0];
                }

                if(!user.followed.includes(followed)) {
                    // Get follower arrays an add the new user to it
                    var followeda = followed.toObject().follower;
                    followeda.push({_id: user.toObject()._id, username: user.toObject().username});
                    var notifications = followed.toObject().notifications;
                    var notifymessage = "Der Benutzer " + user.toObject().name + " folgt dir nun!";
                    var contains = false;
                    for(let i = 0; i < notifications.length; i++) {
                        if(notifications[i].message == notifymessage) {
                            contains = true;
                        }   
                    }
                    if(!contains) notifications.push({message: "Der Benutzer " + user.toObject().name + " folgt dir nun!", seen: false, date: Date.now()});
                    var usera = user.toObject().followed;
                    usera.push({_id: followed.toObject()._id, username: followed.toObject().username});

                    // update user objects
                    process.nextTick(() => { // async
                        user.update({'followed': usera}, (err) => { // update first user
                            process.nextTick(() => { // async
                                followed.update({'follower': followeda, 'notifications': notifications}, (err) => { if(err) console.error(err); }); // update second user
                            });
                        });
                    });
                }
            }
        });
    }
}



function uploadImage(image) {
    client.emit('image', image);
}

module.exports = {
    init,
    uploadImage
}