var passport = require('passport');
var User = require('../model/user');
var LocalStrategy = require('passport-local').Strategy;
var data = require('./data');
var Email = require('../email/email');
var Token = require('../model/token');

var onlineUsers = [];

function startUserActivityTimer() {
    // { _id: id, date: date }
    setInterval(() => {
        var date = Date.now();
        var maxDate = date - (1000*60*10);
        for(let i = 0; i < onlineUsers.length; i++) {
            if(onlineUsers[i].date <= maxDate) {
                onlineUsers.splice(i, 1);
            }
        }
    }, 10000);
}

/* serialize user */
passport.serializeUser(function(user, done) {
    done(null, user.id);
});

/* deserialize user */
passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
        done(err, user);
    });
});

/* passport signup strategy */
passport.use('local-signup', new LocalStrategy({
    usernameField: 'email', // email field
    passwordField: 'password', // password field
    passReqToCallback: true
}, function(req, email, password, done) {
    var username = req.body.username; // username from frontend

    if(username) { // Check if the username isn't null
        // ASYNC
        process.nextTick(function() {

            User.findOne({'email': email}, function(err, user) { // find user with email in the database
                if(err) {
                    return done(err); // if error return 
                }
        
                if(user) { // check if user exists
                    return done(null, false, {message: 'Diese Email wird bereits benutzt!'}); // return error
                } else {

                    User.findOne({'username': username}, (err, userbyusername) => { // find user with username in the database
                        if(err)  
                            return done(err); // if error return

                        if(userbyusername) { // check if user exists
                            return done(null, false, {message: 'Dieser Benutzername wird bereits verwendet!'}); // return error
                        } else {
                            // create new user object
                            const id = data.getNewUUID();
                            var userEmailToken = data.getNewUUID();
                            var verificationURL = "https://woidbook.com/emailverify/" + userEmailToken + "/" + encodeURIComponent(email);

                            var newUser = new User();
                            newUser._id = id;
                            newUser.name = username;
                            newUser.username = username;
                            newUser.email = email;
                            newUser.password = newUser.encryptPassword(password);
                            newUser.confirmEmail = { type: "email", done: false, link: verificationURL, email: newUser.email, expires: (Date.now() + 18000000) };
                            newUser.save(function(err) { // Save data into database
                                if(err) {
                                    return done(err);
                                }
                                    Email.sendMail({
                                        from: "WoidBook Email Confirmation 1@gmail.com",
                                        to: newUser.email,
                                        subject: "Bitte bestätigen Sie Ihre Email!",
                                        html: '<center><h2 style="color: green;">WoidBook</h2></center><br>Klicken Sie auf den folgenden Link um Ihre Email zu bestätigen! \n ' + verificationURL
                                    });
                                return done(null, newUser);
                            });
                        }
                    })

                }
            });
        });
    } else {
        return done(null, false, {message: data.invalidBody});
    }
}));

passport.use('local-login', new LocalStrategy({
    // by default, local strategy uses username and password, we will override with email
    usernameField : 'email',
    passwordField : 'password',
    passReqToCallback : true // allows us to pass back the entire request to the callback
},
function(req, email, password, done) { // callback with email and password from our form

        User.findOne({'email': email}, (err, user) => { // search for user in database
            if(!user) {
                return done(null, false, {message: 'Es existiert kein Account mit dieser Email!'}); // return error if no account exists with this email
            } else {
                if (!user.validPassword(password))
                return done(null, false, {message: 'Falsches Passwort! Bitte versuche es erneut.'}); // return error if the password was wrong

                onlineUsers.push({ _id: user._id, date: Date.now()});
                return done(null, user); // return the user object
            }
        });
}));

module.exports = {
    startUserActivityTimer,
    onlineUsers
}
