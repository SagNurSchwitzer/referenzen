var createError = require('http-errors');
var express = require('express');
var app = express();
var http = require('http').Server(app);
var path = require('path');
var expressHbs = require('express-handlebars');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');
var passport = require('passport');
var mongoose = require('mongoose');
var flash = require('connect-flash');
var socket = require('./config/socket');
var upload = require('express-fileupload');
var email = require('./email/email');
var data = require('./config/data');
var expressJwt = require('express-jwt');

var indexRouter = require('./routes/index');
var userRouter = require('./routes/user');
var postsRouter = require('./routes/posts');
var passwordRouter = require('./routes/password');
var searchRouter = require('./routes/search');
var adminRouter = require('./routes/admin');
var apiRouter = require('./routes/api');
var changelogRouter = require('./routes/changelog');
var verifyemail = require('./routes/verifyemail');

// Connect to database
mongoose.connect(data.databasepassword); 

require('./config/passport');
require('./config/data');

socket.init(http);

// view engine setup
app.engine('.hbs', expressHbs({defaultLayout: 'layout', extname: '.hbs'}));
app.set('view engine', 'hbs');

// Set app configurations
app.use(upload());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(flash());
app.use(session({secret: 'woidbook923578123', resave: false, saveUninitialized: false}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/api', jwt());

// Routers
app.use('/', indexRouter); // index
app.use('/user', userRouter); // user
app.use('/posts', postsRouter); // posts 
app.use('/password', passwordRouter); // password
app.use('/search', searchRouter); // search
app.use('/admin', adminRouter); // admin
app.use('/api', apiRouter); // api
app.use('/changelog', changelogRouter); // changelog
app.use('/emailverify', verifyemail); // verifyemail


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  if (err.name === 'UnauthorizedError') {
      // jwt authentication error
      return res.status(401).json({ message: 'Invalid Token' });
  } else {
    res.status(err.status || 500);
    res.render('error');
  }
});

function jwt() {
  return expressJwt({secret: data.secret}).unless({
      path: [
          // public routes that don't require authentication
          '/api/authenticate',
          '/api/isAuthenticated'
      ]
  });
}

// Set the port and give a callback
http.listen(8000, () => {
  console.log('Woidbook backend started on port 8000');
});

module.exports = app;
